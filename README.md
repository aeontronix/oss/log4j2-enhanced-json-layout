# Aeontronix enhanced json layout

This is a Yet Another log4j2 json layout `ELJsonLayout` which has a couple advantages over the other ones out there:

First, it doesn't has any dependencies which avoid library conflicts (ie all the other json libraries I tried wouldn't work with mule due to conflicts w/ jackson libraries)

# Usage

to use it, add to your project the following dependency:

```xml
<dependency>
    <groupId>com.aeontronix.log4j2</groupId>
    <artifactId>log4j2-enhanced-json-layout</artifactId>
    <version>1.0.2</version>
</dependency>
```

In some cases with complex classloading (ie. mule applications) you might have to specify the package

```xml
<Configuration packages="com.aeontronix.log4j2">
```

then use the layout in your log4j.xml. ie:

```xml
<?xml version="1.0" encoding="utf-8"?>
<Configuration>
    <Appenders>
        <RollingFile name="file" fileName="mytest.log" 
                 filePattern="mytest-%i.log">
            <ELJsonLayout/>
            <SizeBasedTriggeringPolicy size="10 MB" />
            <DefaultRolloverStrategy max="10"/>
        </RollingFile>
    </Appenders>
    <Loggers>
        <AsyncRoot level="INFO">
            <AppenderRef ref="file" />
        </AsyncRoot>
    </Loggers>
</Configuration>
```

or in the case of a mule application

```xml
<?xml version="1.0" encoding="utf-8"?>
<Configuration packages="com.aeontronix.log4j2">
    <Appenders>
        <RollingFile name="file" fileName="mytest.log" 
                 filePattern="mytest-%i.log">
            <ELJsonLayout/>
            <SizeBasedTriggeringPolicy size="10 MB" />
            <DefaultRolloverStrategy max="10"/>
        </RollingFile>
    </Appenders>
    <Loggers>
        <AsyncRoot level="INFO">
            <AppenderRef ref="file" />
        </AsyncRoot>
    </Loggers>
</Configuration>
```


## JSON Message pass-through

The layout parameter 'jsonMessage' allows to automatically consider any message payload that starts with `{` to be considered
as JSON. It will then attempt to parse the message and if successful it will merge the object attributes with the json log
record.

ie: 

```java
logger.info("{\"foo\":\"bar\",\"hello\":\"world\"}");
```

Will result in the log entry

```
{
  "loggerName": "Hello",
  "loggerFqcn": "org.apache.logging.log4j.spi.AbstractLogger",
  "threadName": "main",
  "level": "INFO",
  "message": {
    "foo": "bar",
    "hello": "world"
  },
  "timestamp": "[TIMESTAMP]"
}
```


## MapMessage support

Special support for logj4j MapMessage is included to generate a json message with multiples attributes. 

When using a MapMessage, you can specify that a value in the message map should be included as "raw json" by adding another key/value with the
same key appended with "\_$\_rawjson\_$\_", with the value of "true"

so for example the following key/values:

```
foo=bar
obj={}
```

would generate:

```
{
    ...
    "foo": "bar",
    "obj": "{}"
}
```

but if instead you specify

```
foo=bar
obj={}
obj_$_rawjson_$_=true
```

it would generate:

```
{
    ...
    "foo": "bar",
    "obj": {}
}
```
